#!/bin/bash
source ./config.sh
source ./utils.sh

# WHEN AND HOW TO RUN-------------------------------------
# this is the SECOND script you should run. once you are done with
# the pre-install script and you've chrooted into the root, you
# should run this script. after youve run it, youll be left with a 
# barebones but functional arch installation


# =============================================================
# INITIAL ROOT FUNCTIONS
# =============================================================
# root_locale_conf--------------------------------------------
# configures locale as specified by variables
# CHOSEN_LOCALES and CHOSEN_LANG
function root_locale_conf() {
    # set locales
    cp /etc/locale.gen $LOCALE_GEN_FILE

    # uncomment all chosen locales
    for (( i = 0; i < ${#CHOSEN_LOCALES[@]}; i++ )); do 
        l=${CHOSEN_LOCALES[i]}
        echo "locale $l chosen"
        # see [3]
        sed -i "/^#*$l/s/^#*//" $LOCALE_GEN_FILE 
    done
    cp $LOCALE_GEN_FILE /etc/locale.gen
    locale-gen  



    # set lang
    echo "LANG=$CHOSEN_LANG" > $LOCALE_FILE
    chmod 644 $LOCALE_FILE
    cp $LOCALE_FILE /etc/locale.conf
}
# -----------------------------------------------------------


# root_network_conf-------------------------------------------
# configures hosts file and hostname. also enables networkmanager
function root_network_conf() {
    # set hostname
    echo "$CHOSEN_HOSTNAME" > $HOSTNAME_FILE
    chmod 644 $HOSTNAME_FILE
    cp $HOSTNAME_FILE /etc/hostname



    # set localhost hosts, append to the file
    # in order to keep the neat lil msg at the top
    cp /etc/hosts $HOSTS_FILE
    echo \
"
127.0.0.1	localhost
::1	        localhost
127.0.0.1	$CHOSEN_HOSTNAME.localdomain $CHOSEN_HOSTNAME 
" >> $HOSTS_FILE
    cp $HOSTS_FILE /etc/hosts



    systemctl enable NetworkManager
    groupadd network
    
}
# -----------------------------------------------------------


# root_grub_initramfs_conf-----------------------------------
# installs and configures grub as well as an initramfs. the latter
# is required to de-crypt the root partition. it also adds the 
# necessary grub parameters and additional ones specified by 
# ADDITIONAL_GRUB_PARAMS right AFTER  them

function root_grub_initramfs_conf() {
    # create a proper /etc/mkinitcpio.conf
    cp /etc/mkinitcpio.conf $MKINITCPIO_FILE

    # we have to add encrypt to HOOKS, just replace the whole line....
    hooks="HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)"
    sed -i "s/^HOOKS=.*/$hooks/g" $MKINITCPIO_FILE
    cp $MKINITCPIO_FILE /etc/mkinitcpio.conf

    mkinitcpio -P

    # create a /etc/defaults/grub with the right parameters
    cp /etc/default/grub $GRUB_FILE
    root_partition_uuid=$(blkid -o value -s UUID $ROOT_PARTITION)
    grub_args="cryptdevice=UUID=$root_partition_uuid:luks_root root=/dev/mapper/luks_root $ADDITIONAL_GRUB_PARAMS"

    # see [2 & 4], had to add \" to escape bash quoting. looks horrible. the @'s
    # are there to prevent confusion since grub args might include slashes...but
    # theyre not the best solution if one of the arguments has a @     
    #sed -i "/^GRUB_CMDLINE_LINUX=/s@\"\(.*\)\"@\"\1 $grub_args\"@g" $GRUB_FILE
    
    # awk version
    # still ugly because of mandatory escape sequences, but at least separator
    # characters arent an issue anymore....right?
    awk -i inplace \
    "{
        if (\$0 ~ /^GRUB_CMDLINE_LINUX=\\42.*\\42/) 
            \$0 = gensub(/\\42(.*)\\42/, \"\\42\\\1 $grub_args\\42\", \"g\", \$0)    

        print \$0
    }" $GRUB_FILE

    
    cp $GRUB_FILE /etc/default/grub

    # install grub
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
    grub-mkconfig -o /boot/grub/grub.cfg
}
# -----------------------------------------------------------



function root_main_setup() {
    # create CONF_DIR if it doesnt exist
    mkdir $CONF_DIR
    
    # config network and locale
    root_locale_conf
    root_network_conf

    # install other packages
    pacman -Syu --noconfirm linux-headers efibootmgr \
    grub sudo vi zsh git $ADDITIONAL_SOFTWARE_INITIAL_ROOT 

    # configure local time
    ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime
    hwclock --systohc


    # generate initramfs
    root_grub_initramfs_conf


    # see [5], set wheel group permissions.
    # this way i still use visudo....
    mkdir -p /etc/sudoers.d
    echo '%wheel ALL=(ALL) ALL' | EDITOR='tee -a' visudo -f /etc/sudoers.d/01_wheel
    chmod 0440 /etc/sudoers /etc/sudoers.d/*
    sudo chown root:root /etc/sudoers.d/*

    # add a user
    useradd -m -G wheel,network -s /usr/bin/zsh $USER_NAME

    # set passwords
    echo \
"up next youre gonna set the passwords for root
and the non-root user. if you mess up, just do: 

passwd 
passwd $USER_NAME

after the script is done running
"
    echo "enter root's password: "
    passwd 

    echo "enter $USER_NAME's password: "
    passwd $USER_NAME


    # copy self to the non-root user's home directory
    new_location="/home/$USER_NAME/$( basename $( pwd ) )"
    echo "moving to $new_location"
    mv $( pwd ) $new_location
    chown -R $USER_NAME:$USER_NAME $new_location
    
}
# =============================================================
# END OF INITIAL ROOT FUNCTIONS
# =============================================================


root_main_setup

echo "----------"
echo \
"at this point, you may want to umount -R /mnt and
reboot to see if everything works. then log in as 
$USER_NAME. you will find the scripts in your home folder
and you may run run $C_SCRIPT

TIP: to connect to wifi, try the nmtui command"
