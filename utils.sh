#!/bin/bash

function yn_prompt() {
    while true; do
        read -p "$1" -n 1 -r >&2
        
        if [[ "$REPLY" =~ ^[Yy]$ ]]; then
            echo "true"
            break
        elif [[ "$REPLY" =~ ^[Nn]$ ]]; then
            echo "false"
            break
        fi

        echo "please enter y or n" >&2
    done
}


# this is probably some highly unsafe shit
function passwd_prompt() {
    local -n passwd_var=$2

    while true; do 
        read -p "$1" -r -s first_try >&2
        echo ''>&2

        read -p "enter passwd again: " -r -s second_try >&2
        echo ''>&2

        if [ $first_try = $second_try ]; then
            passwd_var=$first_try
            break
        fi

        echo "passwds do not match" >&2
    done
}