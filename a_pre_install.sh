#!/bin/bash
source ./config.sh
source ./utils.sh

# WHEN AND HOW TO RUN-------------------------------------
# this is the first script you would run. run it as soon as
# you get into the installation media terminal and youve managed
# to connect to the internet. after youve run this script, you should
# have a properly partitioned disk set up with encryption that you can
# now install arch linux in. 

# =============================================================
# PRE INSTALL FUNCTIONS 
# =============================================================
ENCRYPT_PASS=

# pre_install_disk_partitioning----------------------------
# this function will only run if PARTITION_DISKS is set to true.
# its job is to partition the disk specific in DISK_NAME. it 
# creates 2 partitions: boot and root. 
# WARNING: IT WILL ZERO-OUT THE WHOLE DISK BEFORE PARTITIONING.
# RUN IT ONLY IF YOU DONT CARE ABOUT ANYTHING ON THAT DISK

function pre_install_disk_partitioning {
    # wipe everything
    dd if=/dev/random of=$DISK_NAME bs=1M status=progress

    # create partitions
    (   
        echo g

        # partition 1: boot
        echo n
        echo 1
        echo ''
        echo '+1024M'    
        echo t 
        echo uefi
	echo ''

        #partition 2: root
        echo n
        echo 2 
        echo ''
        echo ''

        echo w
    ) | fdisk $DISK_NAME


    parts=()
    while read -r -a line; do 
        if [[ "${line[5]}" = "part" ]]; then
            parts+=( ${line[0]} )
        fi
    done <<< "$( lsblk --list "$DISK_NAME" )"

    BOOT_PARTITION="/dev/${parts[0]}"
    ROOT_PARTITION="/dev/${parts[1]}"
}
# -----------------------------------------------------------

# read_part_name---------------------------------------------
# a utility function to check that the partition names are 
# correct in case they were manually entered
function read_part_name() {
    # see [6]
    local -n part=$1

    ok=false
    if [[ ! -z "$part" ]]; then
        ok=$( yn_prompt "$1=$part, is this okay? [y/n]: " )
    fi  

    if [[ "$ok" = false ]]; then
        read -p "enter the full /dev/ path for $1: " -r part   
    fi 
}
# -----------------------------------------------------------

# pre_install_disk_setup-------------------------------------
# this function makes the necessary filesystems and mounts them
# it also calls pre_install_disk_partitioning or read_part_name
# as required

function pre_install_disk_setup() {
    # pre_install_disk_partitioning will wipe the
    # DISK_NAME disk and create partitions. you need to have
    # the following  
    # >=512mb boot partition, type = EFI
    # [rest of disk] root partition, type = Linux Filesystem
    if [ "$PARTITION_DISKS" = true ] ; then
        pre_install_disk_partitioning    
    else 
        lsblk 
        echo "---------------------"
        read_part_name BOOT_PARTITION
        read_part_name ROOT_PARTITION
    fi

    passwd_prompt "enter encryption passwd: " ENCRYPT_PASS  
    # encrypt root partition
    echo $ENCRYPT_PASS | cryptsetup -q luksFormat -v -s 512 -h sha512 $ROOT_PARTITION
    echo $ENCRYPT_PASS | cryptsetup open $ROOT_PARTITION luks_root


    # format partitions & mount em
    mkfs.vfat -F 32 $BOOT_PARTITION
    mkfs.ext4 /dev/mapper/luks_root
    mount /dev/mapper/luks_root /mnt 
    mkdir /mnt/boot
    mount $BOOT_PARTITION /mnt/boot

    # create swap file
    dd if=/dev/zero of=/mnt/swap bs=1M count=$SWAP_SIZE_MB
    mkswap /mnt/swap
    swapon /mnt/swap 
    chmod 0600 /mnt/swap 

}
# -----------------------------------------------------------

function pre_install_main() {
    # update clock/etc/locale.conf
    timedatectl set-ntp true

    # set up the disks
    pre_install_disk_setup

    # get mirrors
    reflector --latest $MIRROR_COUNT --protocol http --protocol \
     https --sort rate --save /etc/pacman.d/mirrorlist

    # install software
    pacstrap /mnt base base-devel linux linux-firmware \
    nano networkmanager dosfstools $MICROCODE_PACKAGE
     
    # generate fstab
    genfstab -U /mnt >> /mnt/etc/fstab

    # replace partition names in config.sh
    awk -i inplace \
    "{
        if (\$0 ~ /^ROOT_PARTITION=.*/) 
            print \"ROOT_PARTITION=$ROOT_PARTITION\"    
        else if (\$0 ~ /^BOOT_PARTITION=.*/) 
            print \"BOOT_PARTITION=$BOOT_PARTITION\"
        else 
            print \$0
    }" $CONFIG_SCRIPT
    
    # copy self to /mnt, where root is
    new_location="/mnt/$( basename $( pwd ) )"
    echo "copying to $new_location"
    
    mkdir $new_location 
    cp -r $( pwd )/* $new_location
}
# =============================================================
# END OF PRE INSTALL FUNCTIONS 
# =============================================================

# run main function
pre_install_main

echo "----------"
echo \
"now type: arch-chroot /mnt and hit ENTER.
in the root of this environment you will find
this directory again, now you can run $B_SCRIPT"
