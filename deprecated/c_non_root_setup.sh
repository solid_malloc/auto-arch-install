#!/bin/bash
source ./config.sh
source ./utils.sh

# getty_autologin-----------------------------------------------
# this function enables auto-login with getty. by running it,
# you make it so that the  user is automatically logged-in,
# and dropped into a tty. 

# do not run this function if you use other auto-login methods like
# lightdm

function getty_autologin() {
    # need to escape the $ because the double quotes in the sh command
    curr_user_name="$USER_NAME"
    override_content=\
        '[Service]
            ExecStart=
            ExecStart=-/usr/bin/agetty --autologin $curr_user_name --noclear %I \$TERM'

            getty_dir='/etc/systemd/system/getty@tty1.service.d'
            echo $USER_PASS | sudo -k -S mkdir -p $getty_dir
            echo $USER_PASS | sudo -k -S touch $getty_dir/override.conf

            echo $USER_PASS | sudo -k -S sh -c \
                "curr_user_name=\"$curr_user_name\";echo -e \"$override_content\" > $getty_dir/override.conf"
            }
#  -------------------------------------------------------------



# st_setup-------------------------------------------------------
# this function just installs st and patches it. the patches included
# just enable scrolling with the mouse-wheel. if you use another terminal
# emulator, there's no need to call this function. you can find more 
# patches at the suckless website, i just got the ones i use.

function st_setup() {
    # clone st repo and relevant files
    git clone https://git.suckless.org/st
    cp ./conf_files/st/* ./st

    cd ./st

    # patch, compile, and install
    patch -p1 < auto-arch-install-st-patch.diff
    patch -p1 < st-scrollback-20210507-4536f46.diff
    patch -p1 < st-scrollback-mouse-20191024-a2c479c.diff
    patch -p1 < st-scrollback-mouse-altscreen-20200416-5703aa0.diff
    cp ./config.def.h ./config.h

    make
    echo $USER_PASS | sudo -k -S make install 
    cd ..
}
# --------------------------------------------------------------

# dwm_dmenu_clipmenu_setup-----------------------------------------------
# sets up and installs dwm, dmenu, and clipmenu. 

function dwm_dmenu_clipmenu_setup() {
    # dwm compile and install -------------------
    git clone https://git.suckless.org/dwm
    cp ./conf_files/dwm/auto-arch-install-dwm-patch.diff ./dwm 

    cd ./dwm 
    patch < ./auto-arch-install-dwm-patch.diff
    cp ./config.def.h ./config.h
    make
    echo $USER_PASS | sudo -k -S make install 
    cd ..

    # dynamically update scripts location
    awk -i inplace \
        "{ gsub(/USER_SCRIPTS_DIR/,\"$USER_SCRIPTS_DIR\"); print }" \
        ./conf_files/dwm/run_dwm

    # copy them to the specified location
    chmod 744 ./conf_files/dwm/{statusbar,run_dwm} 
    cp ./conf_files/dwm/{statusbar,run_dwm} $USER_SCRIPTS_DIR
    # -------------------------------------------

    # dmenu compile and install -----------------
    git clone https://git.suckless.org/dmenu
    cp ./conf_files/dmenu/auto-arch-install-dmenu-patch.diff ./dmenu 

    cd ./dmenu 
    patch < ./auto-arch-install-dmenu-patch.diff
    cp ./config.def.h ./config.h
    make
    echo $USER_PASS | sudo -k -S make install 
    cd ..
    # -------------------------------------------

    # clipmenu install --------------------------
    echo $USER_PASS | sudo -k -S pacman -Syu --noconfirm clipmenu
    # -------------------------------------------
}
# --------------------------------------------------------------


# dwm_lightdm_setup---------------------------------------------
# installs lightdm, enables it, and sets it up to use dwm and autologin
# for the current user. it ofc depends on having dwm installed, and
# makes use of the included run_dwm script

function dwm_lightdm_setup() {
    # if ya set this up, then dont set up autologin with getty

    # lightdm setup
    echo $USER_PASS | sudo -k -S pacman -Syu --noconfirm lightdm

    echo $USER_PASS | sudo -k -S systemctl enable lightdm

    # set the appropriate values in lightdm.conf
    # these are: 
    # - autologin-user=[user to be auto-logged-in]
    # - autologin-session=[dwm, change it if you use another DE]
    # - logind-check-graphical=true
    # without that last one it will scream about not having a
    # greeter. 
    # there is probably a much better way of doing this
    cp /etc/lightdm/lightdm.conf ./conf_files/dwm/
    awk -i inplace \
        " BEGIN { in_seat = 0; in_lightdm = 0; }
            { 
                if ( \$0 ~ /^\[LightDM\]$/ ) in_lightdm = 1;
                else if ( \$0 ~ /^\[Seat:\*\]$/) { in_seat = 1;  in_lightdm = 0; }
                else if ( \$0 ~ /^\[XDMCPServer\]$/) in_seat = 0;

                    if ( in_seat == 1 && \$0 ~ /^#*autologin-user=.*/)
                        print \"autologin-user=$USER_NAME\"
                    else if ( in_seat == 1 && \$0 ~ /^#*autologin-session=.*/ )    
                        print \"autologin-session=dwm\"
                    else if ( in_lightdm == 1 && \$0 ~ /^#*logind-check-graphical=.*/ )
                        print \"logind-check-graphical=true\"
                    else 
                        print \$0

                    }" ./conf_files/dwm/lightdm.conf

                    echo $USER_PASS | sudo -k -S cp \
                        ./conf_files/dwm/lightdm.conf /etc/lightdm/lightdm.conf



    # dynamically update scripts location
    awk -i inplace \
        "{ gsub(/USER_SCRIPTS_DIR/,\"$USER_SCRIPTS_DIR\"); print }" \
        ./conf_files/dwm/dwm.desktop

    echo $USER_PASS | sudo -k -S groupadd -r autologin
    echo $USER_PASS | sudo -k -S gpasswd -a "$USER_NAME" autologin

    echo $USER_PASS | sudo -k -S mkdir -p /usr/share/xsessions 
    echo $USER_PASS | sudo -k -S cp ./conf_files/dwm/dwm.desktop /usr/share/xsessions
    echo $USER_PASS | sudo -k -S chmod 644 /usr/share/xsessions/dwm.dekstop
}
# --------------------------------------------------------------


# clang_format_setup--------------------------------------------
# sets up and installs clang-format. at the time of writing, i am
# very fond of this program, but i know from experience that acquiring 
# it requires either installing clang or building it by hand, instead
# i package it in there, considering it's a pretty self-contained thing. 
function clang_format_setup() {

    if [ "$BUILD_CLANG_FORMAT" = true ] ; then
        git clone https://github.com/llvm/llvm-project.git
        mkdir llvm-project/build
        cd llvm-project/build
        cmake \
            -DCMAKE_BUILD_TYPE=MinSizeRel \
            -DLLVM_ENABLE_PROJECTS=clang \
            -DLLVM_TARGET_ARCH=x86_64 \
            -DLLVM_DEFAULT_TARGET_TRIPLE=x86_64-apple-darwin \
            -DCMAKE_OSX_ARCHITECTURES=x86_64 \
            ../llvm;

        make $BUILD_CLAG_OPTIONS
        cp ./bin/clang-format $USER_SCRIPTS_DIR
	    cd ../..
    else
        # copy the clang format binary and
        cp -r ./conf_files/user_bins/clang-format $USER_SCRIPTS_DIR
    fi
}
# --------------------------------------------------------------