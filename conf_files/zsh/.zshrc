# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd beep extendedglob nomatch
unsetopt notify
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall


# PLUGINS----------------------------------
# this one should go under syntax highlight
source ~/.zsh/zsh-history-substring-search/zsh-history-substring-search.zsh
# -----------------------------------------



# PROMPT CONFIG-------------------------------

# https://git-scm.com/book/en/v2/Appendix-A%3A-Git-in-Other-Environments-Git-in-Zsh
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%b'

# http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html
PROMPT_BG='#cfcff6'
PROMPT="%F{$PROMPT_BG}%B%S[%*]%s%f%b %2~ ? "
# -----------------------------------------



# EXPORTS----------------------------------
export PATH="$HOME/.local/bin:$PATH"
export LS_COLORS='di=1;36:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43'
# -----------------------------------------



# KEYBINDS---------------------------------
# detected using sed e
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey "^[[3~" delete-char
#------------------------------------------

# ALIASES----------------------------------
alias ls="ls --color=auto"
alias l="ls"
alias la="ls -la"
alias hist="history -f 0"

caps2ctrl() {
    if [ -n "$WAYLAND_DISPLAY" ]; then
        if gsettings list-recursively | \
           grep -q 'org.gnome.desktop.input-sources xkb-options'; then

            gnome_xkb_options=$(gsettings get org.gnome.desktop.input-sources \
                                xkb-options)
    
            if [[ "$gnome_xkb_options" == *"ctrl:nocaps"* ]]; then
                gsettings set org.gnome.desktop.input-sources xkb-options "[]"
                echo "layout reset"
            else
                gsettings set org.gnome.desktop.input-sources xkb-options \
                          "['ctrl:nocaps']"
                
                echo "caps set to ctrl"
            fi

        else
            echo "you're using wayland but not gnome, you're not a sexual \
                  deviant"
        fi
    else
        state=$(setxkbmap -query | grep ctrl:nocaps)
        if [ "$state" = "" ]; then 
            setxkbmap -option ctrl:nocaps	    	
            echo "caps set to ctrl"	
        else
            setxkbmap -option
            echo "layout reset"
        fi 
    fi
}

toggle_keyboard_layout() {
  current_layout=$(setxkbmap -query | grep layout | awk '{print $2}')

  if [ "$current_layout" = "us" ]; then
    setxkbmap ru
  else
    setxkbmap us
  fi
}
# -----------------------------------------



