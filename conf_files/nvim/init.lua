-- PLUGIN CONFIG **************************************************************


-- pckr -----------------------------------------
local function bootstrap_pckr()
    local pckr_path = vim.fn.stdpath("data") .. "/pckr/pckr.nvim"

    if not (vim.uv or vim.loop).fs_stat(pckr_path) then
        vim.fn.system({
            'git',
            'clone',
            "--filter=blob:none",
            'https://github.com/lewis6991/pckr.nvim',
            pckr_path
        })
    end

    vim.opt.rtp:prepend(pckr_path)
end

bootstrap_pckr()


require('pckr').add {
    'lukas-reineke/indent-blankline.nvim';
    'gennaro-tedesco/nvim-commaround';
    'lewis6991/gitsigns.nvim';
    'matbme/JABS.nvim';
}
-------------------------------------------------

require("ibl").setup() -- indent-blankline
require('gitsigns').setup() -- gitsigns
require('jabs').setup {
    symbols = {
        current = "C",
        split = "S",
        alternate = "A",
        hidden = "H",
        locked = "L",
        ro = "R",
        edited = "E",
        terminal = "T",
        default_file = "D",
        terminal_symbol = ">_"
    },

}

-- nvim-commaround
vim.keymap.set('v', '<C-/>', '<Plug>ToggleCommaround', { noremap = true, silent = true })

-- jabs open
vim.keymap.set('n', '<C-b>', ':JABSOpen<CR>', { noremap = true, silent = true })
vim.keymap.set('v', '<C-b>', ':JABSOpen<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-b>', '<ESC>:JABSOpen<CR>', { noremap = true, silent = true })

-- ****************************************************************************

-- BASIC THINGS ***************************************************************
vim.cmd("syntax on")
vim.o.number        = true
vim.filetype.plugin = true
vim.filetype.indent = true

vim.o.tabstop       = 4    -- show existing tab characters as 4 spaces wide
vim.o.shiftwidth    = 4    -- indentation width for '>' and '<'
vim.o.expandtab     = true -- convert tabs to spaces when typing

vim.o.laststatus    = 2
vim.o.undofile      = true
vim.o.hidden        = true

vim.cmd("colorscheme jellybeans") -- set colorscheme

-- turn off modifiable for read-only mode
vim.api.nvim_create_autocmd("BufRead", {
    pattern = "*",
    callback = function()
        vim.bo.modifiable = not vim.bo.readonly
    end,
})

vim.o.ruler       = true
vim.o.startofline = false
vim.o.hlsearch    = true


-- netrw -----------------------------------------
vim.g.netrw_banner = 0
vim.g.netrw_browse_split = 4
vim.g.netrw_altv = 1
vim.g.netrw_winsize = 25
-- -----------------------------------------------

-- works in st-----------------------------------
vim.keymap.set('i', '<M-s>', '<Esc>:w<CR>i', { noremap = true, silent = true })
vim.keymap.set('n', '<M-s>', '<Esc>:w<CR>', { noremap = true, silent = true })
-------------------------------------------------

-- soft line navigation -------------------------
vim.keymap.set('n', '<Up>', 'gk', { noremap = true, silent = true })
vim.keymap.set('n', '<Down>', 'gj', { noremap = true, silent = true })
vim.keymap.set('i', '<Up>', '<C-o>gk', { noremap = true, silent = true })
vim.keymap.set('i', '<Down>', '<C-o>gj', { noremap = true, silent = true })
-------------------------------------------------

-- paste, cut, copy -----------------------------
vim.keymap.set('i', '<C-P>', '<C-R><C-O>+', { noremap = true, silent = true })
vim.keymap.set('c', '<C-P>', '<C-R>+', { noremap = true, silent = true })
vim.keymap.set('v', 'd', '"+d', { noremap = true, silent = true })
vim.keymap.set('v', 'y', '"+y', { noremap = true, silent = true })
-------------------------------------------------

-- visual mode mappings -------------------------
vim.keymap.set('v', 'k', 'e', { noremap = true, silent = true })
vim.keymap.set('v', 'j', 'ge', { noremap = true, silent = true })
vim.keymap.set('v', 'l', '$', { noremap = true, silent = true })
vim.keymap.set('v', 'h', '0', { noremap = true, silent = true })
-------------------------------------------------

-- backspace and cursor keys wrap to previous/next line
vim.o.backspace = "indent,eol,start"
vim.o.whichwrap = vim.o.whichwrap .. ",<,>,[,]"
-------------------------------------------------

--  quick select mode ---------------------------
vim.keymap.set('i', '<C-V>', '<C-O>v', { noremap = true, silent = true })
-------------------------------------------------

-- undo and redo in insert ----------------------
vim.keymap.set('i', '<C-U>', '<C-O>u', { noremap = true, silent = true })
vim.keymap.set('i', '<C-R>', '<C-O><C-R>', { noremap = true, silent = true })
-------------------------------------------------

-- backspace in visual mode deletes selection ---
vim.keymap.set('v', '<BS>', 'd', { noremap = true, silent = true })
-------------------------------------------------

-- undo and redo in insert ----------------------
vim.keymap.set('i', '<C-U>', '<C-O>u', { noremap = true, silent = true })
vim.keymap.set('i', '<C-R>', '<C-O><C-R>', { noremap = true, silent = true })
-------------------------------------------------

-- poor man's emacs style undo and redo ---------
-- requires these mappings.
-- different, better solution:
-- https://vi.stackexchange.com/questions/2376/how-to-change-undo-granularity-in-vim

vim.keymap.set('i', '<CR>', '<CR><C-G>u', { noremap = true, silent = true })
vim.keymap.set('i', '<Space>', '<Space><C-G>u', { noremap = true, silent = true })
-------------------------------------------------

-- buffer navigation ----------------------------
vim.keymap.set('n', '<C-K>', ':bnext<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '<C-J>', ':bprev<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-K>', '<Esc>:bnext<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<C-J>', '<Esc>:bprev<CR>i', { noremap = true, silent = true })
-------------------------------------------------

-- split and close windows ----------------------
vim.keymap.set('i', '<C-W>v', '<C-O><C-W>v', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W>s', '<C-O><C-W>s', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W>c', '<C-O><C-W>c', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W>o', '<C-O><C-W>o', { noremap = true, silent = true })
-------------------------------------------------

-- navigate between windows ---------------------
vim.keymap.set('i', '<C-W><Left>', '<C-O><C-W><Left>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W><Right>', '<C-O><C-W><Right>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W><Up>', '<C-O><C-W><Up>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W><Down>', '<C-O><C-W><Down>', { noremap = true, silent = true })
-------------------------------------------------

-- next and prev windows ------------------------
vim.keymap.set('n', '<M-k>', '<C-W>w', { noremap = true, silent = true })
vim.keymap.set('n', '<M-j>', '<C-W>W', { noremap = true, silent = true })
vim.keymap.set('i', '<M-k>', '<C-O><C-W>w', { noremap = true, silent = true })
vim.keymap.set('i', '<M-j>', '<C-O><C-W>W', { noremap = true, silent = true })
-------------------------------------------------

-- change size of windows -----------------------
vim.keymap.set('n', '<C-h>', ':vertical resize -2<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '<C-l>', ':vertical resize +2<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-h>', '<Esc>:vertical resize -2<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<C-l>', '<Esc>:vertical resize +2<CR>i', { noremap = true, silent = true })
-------------------------------------------------

-------------------------------------------------
vim.keymap.set('n', '<M-h>', ':resize -2<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '<M-l>', ':resize +2<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<M-h>', '<Esc>:resize -2<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<M-l>', '<Esc>:resize +2<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W>-', '<Esc>:vertical resize -2<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W>=', '<Esc>:vertical resize +2<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W><', '<Esc>:resize -2<CR>i', { noremap = true, silent = true })
vim.keymap.set('i', '<C-W>>', '<Esc>:resize +2<CR>i', { noremap = true, silent = true })
-------------------------------------------------

-- ctrl-f is the search -------------------------
vim.keymap.set('i', '<C-F>', '<Esc>/', { noremap = true, silent = true })

-- reload file ----------------------------------
vim.keymap.set('n', '<C-E>', ':e<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<C-E>', '<Esc>:e<CR>i', { noremap = true, silent = true })
-------------------------------------------------

-- ****************************************************************************

-- FUNCTIONS FOR MORE COMPLEX STUFF *******************************************
-- function to perform find and replace ---------
local function find_and_replace()
    local str1 = vim.fn.input('find: ')
    if str1 == "" then return end

    local str2 = vim.fn.input('replace: ')
    if str2 == "" then return end

    local mode = vim.fn.visualmode()
    if mode ~= "" then
        vim.api.nvim_command(string.format("'<,'>s/%s/%s/g", str1, str2))
    else
        vim.api.nvim_command(':%s/' .. str1 .. '/' .. str2 .. '/g')
    end
end

vim.keymap.set('n', '<M-f>', find_and_replace, { noremap = true, silent = true })
vim.keymap.set('v', '<M-f>', find_and_replace, { noremap = true, silent = true })
vim.keymap.set('i', '<M-f>', find_and_replace, { noremap = true, silent = true })
-------------------------------------------------

-- quick mark jumping from insert ---------------
local function set_new_mark()
    local markname = vim.fn.input('markname: ')
    if markname == "" then return end
    vim.api.nvim_command('ma' .. markname)
    vim.api.nvim_feedkeys('i', 'n', false)
end

local function jump_to_mark()
    local markname = vim.fn.input('markname: ')
    if markname == "" then return end
    vim.api.nvim_feedkeys('`' .. markname, 'n', false)
    vim.api.nvim_feedkeys('i', 'n', false)
end

vim.keymap.set('i', '<M-a>', set_new_mark, { noremap = true, silent = true })
vim.keymap.set('i', '<C-A>', jump_to_mark, { noremap = true, silent = true })
-------------------------------------------------

-- close buffers normally -----------------------
local function close_current_buf()
    local target_buf = vim.api.nvim_get_current_buf()

    if target_buf < 0 then
        vim.api.nvim_err_writeln("no matching buffer for " .. target_buf)
        return
    end

    if vim.api.nvim_buf_get_option(target_buf, 'modified') then
        vim.api.nvim_out_write("no write since last change for buffer")
        return
    end

    local buf_list = vim.api.nvim_list_bufs()
    local windows = vim.api.nvim_list_wins()

    -- try last edited first
    local new_buf = vim.fn.bufnr('#')

    if new_buf == target_buf or new_buf == -1 then    
        -- if the one you want to kill is also the last edited, then just find
        -- whatever
        if #buf_list == 1 then
            vim.api.nvim_command('quit')
            return
        else
            for _, buf_id in ipairs(buf_list) do
                if buf_id ~= target_buf then
                    new_buf = buf_id
                    break
                end
            end        
        end

    end
    
    for _, win_id in ipairs(windows) do
        local buf_id = vim.api.nvim_win_get_buf(win_id)
        if buf_id == target_buf then
            vim.api.nvim_win_set_buf(win_id, new_buf)        
        end    
    end


    vim.api.nvim_buf_delete(target_buf, {force = false})

end

vim.keymap.set('n', '<M-c>', close_current_buf, { noremap = true, silent = true })
vim.keymap.set('i', '<M-c>', close_current_buf, { noremap = true, silent = true })
-------------------------------------------------

-- ****************************************************************************



-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- THINGS THAT NEED NO PLUGIN BUT I KEEP FORGETTING
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- FIND AND REPLACE ONE BY ONE-------------------
-- 1. search for text (which will put you in normal mode anyway)
-- 2. type cgn (change the next search hit) then immediately type the
--    replacement. this will put you in insert mode, make the edit where
--    the word used to be. when done go back to normal with esc
-- 3. from normal mode, search for the next occurrence that you want
--    to replace (n) and press . to repeat the last change.
-- ----------------------------------------------


-- SHIFTING BLOCKS-------------------------------
-- 1. enter normal mode and select with v the block
-- 2. press > to indent (shift text one 'shiftwidth'
--    to the right), or press < to shift left.
-- 3. press . to repeat the indent, or u to undo if
--    you have shifted too far.
-- 4. type gv if you want to reselect the lines.
-- ----------------------------------------------


-- NETRW SHORTCUTS-------------------------------
-- 1. reload by pressing enter on the `./` option
-- 2. gh to show hidden files
-- 3. gn to switch root directory to the one under
--    the cursor
-- 4. i to switch viewing mode
-- 5. % for new file
-- 6. d for new directory
-- ----------------------------------------------


-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- VIM RESOURCES
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- - https://learnvim.irian.to/
-- - https://www.brianstorti.com/vim-registers/
-- - https://vimdoc.sourceforge.net/htmldoc/intro.html#%3Clt%3E
-- - https://renenyffenegger.ch/notes/development/vim/script/vimscript/functions/map
