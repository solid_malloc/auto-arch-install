#!/bin/bash
source ./config.sh
source ./utils.sh

# WHEN AND HOW TO RUN-------------------------------------
# you can run this script once you've run A_SCRIPT and B_SCRIPT.
# in this script, you should scroll down to the bottom, and comment
# out unwanted set-ups. make sure to read the functionality of 
# every function youre gonna comment out. 

# =============================================================
# INITIAL NON ROOT FUNCTIONS 
# =============================================================

# grant_sudo_nopass --------------------------------------------
function grant_sudo_nopass() {
    echo "$USER_NAME ALL=(ALL) NOPASSWD: ALL" | EDITOR='tee -a' sudo -E visudo -f \
    /etc/sudoers.d/99_$USER_NAME
}
# --------------------------------------------------------------

# yay_setup ----------------------------------------------------
function yay_setup() {
    # clone AUR repo
    sudo pacman -Sy --needed --noconfirm git base-devel
    git clone https://aur.archlinux.org/yay.git
    
    # go into dir, install, leave
    cd yay
    sudo -u $USER_NAME makepkg -si --noconfirm
    cd ..
}
# --------------------------------------------------------------


# video_setup --------------------------------------------------
function video_setup() {
    sudo pacman -Syu --noconfirm $VIDEO_DRIVER libinput acpilight
    sudo usermod -a -G video $USER_NAME
}
# --------------------------------------------------------------

# xorg_setup----------------------------------------------------
# installs xorg and xorg-apps, libinput, the specified video driver
# and acpilight. also sets up a configuration file to reduce tearing,
# depending on the video driver that was installed. 

# some people might argue that xorg-apps is overkill, but i've been
# constantly saved from installing things by hand by xorg-apps' 
# bloatedness. if you choose to remove it from the install command,
# then make sure you install the stuff required for any other functions
# youre gonna run from this script. 

function xorg_setup() {
    # install xorg and video driver
    sudo pacman -Syu --noconfirm $VIDEO_DRIVER
    sudo pacman -Syu --noconfirm xorg-server \
    xorg-apps xorg-xinit xf86-input-libinput acpilight

    # user needs to be part of video group to control backlight
    sudo usermod -a -G video $USER_NAME

    # copy relevant video driver config
    conf_file_name=
    case $VIDEO_DRIVER in

        xf86-video-amdgpu)
            conf_file_name="20-amdgpu.conf"
            ;;

        xf86-video-intel)
            conf_file_name="20-intel.conf"
            ;;
    esac 

    sudo cp ./conf_files/xorg_conf/$conf_file_name /etc/X11/xorg.conf.d/
    sudo chmod 644 /etc/X11/xorg.conf.d/$conf_file_name
}
# --------------------------------------------------------------

# awesomewm_setup-----------------------------------------------
# installs awesomewm and sets up the right configuration
function awesomewm_setup() {
    sudo pacman -Syu --noconfirm awesome
    
    awesomewm_confdir="/home/$USER_NAME/.config/awesome" 
    mkdir -p $awesomewm_confdir     
    cp ./conf_files/awesomewm/rc.lua $awesomewm_confdir
}
# --------------------------------------------------------------

# lxqt_setup------------------------------------------------
# sets up lxqt
function lxqt_setup() {
    sudo pacman -Syu --noconfirm lxqt \
    adwaita-icon-theme
}
# --------------------------------------------------------------

# footfag_setup ------------------------------------------------
# sets up gnome
function footfag_setup() {
    sudo pacman -Syu --noconfirm gdm nautilus gnome-control-center \
    gnome-tweaks gnome-shell-extensions

    yay -Syu --noconfirm gnome-shell-extension-clipboard-indicator

    # enable auto-login for user
    sudo systemctl enable gdm.service
    file_content="\
# see [13]
[daemon]
AutomaticLogin=$USER_NAME
AutomaticLoginEnable=True
"

    mkdir -p ./conf_files/gdm
    echo "$file_content" > ./conf_files/gdm/custom.conf

    sudo mkdir -p /etc/gdm/   
    sudo cp ./conf_files/gdm/custom.conf /etc/gdm/custom.conf

    # black background
    gsettings set org.gnome.desktop.background primary-color 'rgb(0, 0, 0)'
    
    # i like to use ctrl+v for clipboard history, but its taken up by showing
    # the notification tray
    gsettings set org.gnome.shell.keybindings toggle-message-tray \
    "['<Super>m']"
    
    # enable the clipboard history extension
    gnome-extensions enable clipboard-indicator@tudmotu.com

    # reassign the shortcut to display it
    gsettings set org.gnome.shell.extensions.clipboard-indicator toggle-menu \
    "['<Super>v']"
    
    # set max history size
    gsettings set org.gnome.shell.extensions.clipboard-indicator history-size \
    200

    # reset clipboard on boot
    gsettings set org.gnome.shell.extensions.clipboard-indicator clear-on-boot \
    true
    
    # super + e opens up nautilus
    gsettings set org.gnome.settings-daemon.plugins.media-keys \
    custom-keybindings \
    "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
    
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ \
    name 'Open Nautilus'
    
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ \
    command 'nautilus'

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ \
    binding '<Super>e'

    # ctrl + alt + t for ms.alacritty
    gsettings set org.gnome.settings-daemon.plugins.media-keys \
    custom-keybindings \
    "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/']"
    
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ \
    name 'Open Alacritty'

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ \
    command 'alacritty'

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ \
    binding '<Control><Alt>t'

}
# --------------------------------------------------------------

# dmenu_setup---------------------------------------------------
# sets up dmenu
function dmenu_setup() {
    git clone https://git.suckless.org/dmenu

    cd ./dmenu 
    make
    sudo make install 
    cd ..

    # clipmenu install
    sudo pacman -Syu --noconfirm clipmenu
    systemctl --user enable clipmenud.service
}
# --------------------------------------------------------------

# awesomewm_lightdm_setup---------------------------------------
# sets up lightdm to use awesomewm
function awesomewm_lightdm_setup() {
    # if ya set this up, then dont set up autologin with getty

    # lightdm setup
    sudo pacman -Syu --noconfirm lightdm
    sudo systemctl enable lightdm

    # set the appropriate values in lightdm.conf
    # these are: 
    # - autologin-user=[user to be auto-logged-in]
    # - autologin-session=[dwm, change it if you use another DE]
    # - logind-check-graphical=true
    # without that last one it will scream about not having a
    # greeter. 
    # there is probably a much better way of doing this
    cp /etc/lightdm/lightdm.conf ./conf_files/awesomewm/
    awk -i inplace \
        " BEGIN { in_seat = 0; in_lightdm = 0; }
            { 
                if ( \$0 ~ /^\[LightDM\]$/ ) in_lightdm = 1;
                else if ( \$0 ~ /^\[Seat:\*\]$/) { in_seat = 1;  in_lightdm = 0; }
                else if ( \$0 ~ /^\[XDMCPServer\]$/) in_seat = 0;

                    if ( in_seat == 1 && \$0 ~ /^#*autologin-user=.*/)
                        print \"autologin-user=$USER_NAME\"
                    else if ( in_seat == 1 && \$0 ~ /^#*autologin-session=.*/ )    
                        print \"autologin-session=awesome\"
                    else if ( in_lightdm == 1 && \$0 ~ /^#*logind-check-graphical=.*/ )
                        print \"logind-check-graphical=true\"
                    else 
                        print \$0

                    }" ./conf_files/awesomewm/lightdm.conf
    
    sudo cp ./conf_files/awesomewm/lightdm.conf /etc/lightdm/lightdm.conf
    
    # create autologin group and add user to it
    sudo groupadd -r autologin
    sudo gpasswd -a "$USER_NAME" autologin
}
# --------------------------------------------------------------

# lxqt_sddm_setup------------------------------------------------
# sets up SDDM to autologin with lxqt
function lxqt_sddm_setup() {
    sudo pacman -Syu --noconfirm sddm 
    sudo systemctl enable sddm

    file_content="\
[Autologin]
User=$USER_NAME
Session=lxqt"
    
    mkdir -p ./conf_files/sddm
    echo "$file_content" > ./conf_files/sddm/autologin.conf

    sudo mkdir -p /etc/sddm.conf.d/
    sudo cp ./conf_files/sddm/autologin.conf /etc/sddm.conf.d/autologin.conf
    
}
# --------------------------------------------------------------

# alacritty_setup-----------------------------------------------
# this function will set up alacritty. it simply installs it and 
# places my preferred config in the home folder. you can read about
# where else you can put it in alacritty's documentation.

function alacritty_setup() {    
    sudo pacman -Syu --noconfirm alacritty
    cp ./conf_files/alacritty/.alacritty.toml /home/$USER_NAME
}
# --------------------------------------------------------------


# zsh_setup-----------------------------------------------------
# zsh is installed by default by SCRIPT_B, but i still like it 
# set up a specific way, call this function if you'd like to try it
# out. 

function zsh_setup() {
    # copy .zshrc to home
    cp ./conf_files/zsh/.zshrc /home/$USER_NAME

    mkdir -p ~/.zsh

    # install substring history search
    git clone https://github.com/zsh-users/zsh-history-substring-search
    cp -r ./zsh-history-substring-search/ /home/$USER_NAME/.zsh 
}
# --------------------------------------------------------------

# ncpamixer_setup ----------------------------------------------
# sets up a convenient cli curses manager for pulseaudio

function ncpamixer_setup() {
    # clone AUR repo
    git clone https://aur.archlinux.org/ncpamixer.git
    
    # go into dir, install, leave
    cd ncpamixer
    sudo -u $USER_NAME makepkg -si --noconfirm
    cd ..
}
 
# --------------------------------------------------------------

# vim_setup-----------------------------------------------------
# this function compiles and installs vim with my preferred
# configuration. this function can be considered "deprecated", 
# since i don't currently intend in updating the configuration
# anymore. 

function vim_setup() {
    # install vim dependencies
    sudo pacman -Syu --noconfirm ncurses libxext gtk3

    #clone the vim repo
    git clone https://github.com/vim/vim
    cd ./vim 

    # some of the options in this configure cmd require libraries that
    # seem to be installed by one of the things ive already installed.
    # probably xorg-apps. 

    # in case this fails, sources [9] to [12] are a good place to start
    # looking for answers
    ./configure --with-features=huge \
        --enable-multibyte \
        --enable-python3interp=yes \
        --with-python3-config-dir=$(python3-config --configdir) \
        --enable-cscope \
        --with-x \
        --prefix=/usr/local \
        --enable-gui=gtk3

    make 
    sudo make install   
    cd ..

    #-----------------------------------------------------
    # i am aware that most people who would use vim
    # have their own configs, or maybe dont use a config 
    # at all. the rest of this function attempts to install
    # my preferred vim configuration, but comment it if 
    # you'd like. you'll still get a terminal-only vim
    # installation with python3 and clipboard support. 
    #-----------------------------------------------------

    # unpack and setup vim conf files
    cd ./conf_files/vim/
    cp -r .vim /home/$USER_NAME
    cd ../..

    # [8] install plugins
    vim +'PlugInstall --sync' +qa

}
# --------------------------------------------------------------

# neovim_setup -------------------------------------------------
# install neovim in the same way you would normal vim
function neovim_setup() {
    
    nvim_dir="/home/$USER_NAME/.config/"

    # install nvim
    sudo pacman -Syu --noconfirm neovim
    mkdir -p $nvim_dir
    cp -r ./conf_files/nvim/ $nvim_dir

    
}
# --------------------------------------------------------------

# additional_xorg_setup ----------------------------------------
# these used to be separate commands after the end of the file, after all other
# functions, but i don't remember why, so instead i'll put em in this function,
# which can be more conveniently commented. 
function additional_xorg_setup() {
    # enable touchpad tap clicking with libinput
    sudo cp ./conf_files/xorg_conf/30-touchpad.conf \
    /etc/X11/xorg.conf.d/
    
    sudo chmod 644 /etc/X11/xorg.conf.d/30-touchpad.conf
}
# --------------------------------------------------------------


# sudo_setup ---------------------------------------------------
function revoke_sudo_nopass() {
  sudo rm /etc/sudoers.d/99_$USER_NAME
}
# --------------------------------------------------------------


function non_root_main_setup() {
    # make user script dir
    mkdir -p $USER_SCRIPTS_DIR

    # download 
    sudo pacman -Syu --noconfirm

    sudo pacman -Syu --noconfirm \
        alsa-utils alsa-plugins pulseaudio pulseaudio-alsa p7zip udisks2 \
        adobe-source-code-pro-fonts make cmake polkit flatpak firefox

    # disable terminal beeping
    sudo sh -c "echo 'blacklist pcspkr' > /etc/modprobe.d/nobeep.conf"  
    sudo alsactl store
}


function workarounds_1() {
    # 2023-07-23 - gnome and flatpak fuckery --------------
    # installing flatpak along with git results in 
    # gnome-keyring causing a problem whereby gtk
    # applications take forever to start. there are two
    # quick solutions for this that i know of:
    # - install and enable dbus-broker
    # - remove gnome-keyring and all related dependencies
    sudo pacman -Syu --noconfirm dbus-broker
    sudo systemctl enable dbus-broker.service
    sudo systemctl --global enable dbus-broker.service
}


# =============================================================
# END OF INITIAL NON ROOT FUNCTIONS 
# =============================================================


grant_sudo_nopass
trap revoke_sudo_nopass EXIT

non_root_main_setup
workarounds_1

yay_setup

# run only one of these two
video_setup
# xorg_setup

# awesomewm_setup
# awesomewm_lightdm_setup

# lxqt_setup
# lxqt_sddm_setup

footfag_setup

# dmenu_setup

alacritty_setup
zsh_setup
# ncpamixer_setup
# vim_setup

# additional_xorg_setup

trap - EXIT
revoke_sudo_nopass
