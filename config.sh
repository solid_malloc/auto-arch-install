#!/bin/bash

# HOW TO USE THIS-------------------------------------
# this script is where you set all your variables. 
# unless it is otherwise indicated by a comment, it is 
# necessary that you set all of these.
# after setting all of these, run A_SCRIPT.

echo "[TODO: ADD AN EXAMPLE CONFIG]"
echo "[TODO: INSERT TESTED WAYS OF GETTING THE REPO HERE OR IN THE README]"
echo "[TODO: ADD A HELP OPTION THAT PRINTS INSTRUCTIONS FOR EACH SCRIPT]"
echo "[TODO: ADD COLORFUL PROMPTS THAT INDICATE WHAT IS CURRENTLY BEING DONE]"
echo "[TODO: ADD A LOG FILE]"
echo "[TODO: MAKE A AND B INTO A SINGLE SCRIPT"


# GUIDES AND FORUMS REFERENCED
# 1. https://medium.com/hacker-toolbelt/arch-install-with-full-disk-encryption-6192e9635281
# 2. https://unix.stackexchange.com/questions/137030/how-do-i-extract-the-content-of-quoted-strings-from-the-output-of-a-command
# 3. https://stackoverflow.com/questions/24889346/how-to-uncomment-a-line-that-contains-a-specific-string-using-sed/24889374
# 4. https://linuxize.com/post/how-to-use-sed-to-find-and-replace-string-in-files/
# 5. https://stackoverflow.com/questions/323957/how-do-i-edit-etc-sudoers-from-a-script
# 6. https://stackoverflow.com/questions/540298/passing-arguments-by-reference
# 7. https://linuxtechlab.com/bash-scripting-learn-use-regex-basics/
# 8. https://github.com/junegunn/vim-plug/issues/675
# 9. https://github.com/ycm-core/YouCompleteMe/wiki/Building-Vim-from-source
# 10. https://superuser.com/questions/708245/re-compile-vim-with-options
# 11. https://stackoverflow.com/questions/11416069/compile-vim-with-clipboard-and-xterm
# 12. https://github.com/vim/vim/blob/master/src/INSTALL
# 13. https://wiki.archlinux.org/title/GDM#Automatic_login

# script files++++++++++++++++++++++++++++++++++
CONFIG_SCRIPT="./config.sh"
A_SCRIPT="./a_pre_install.sh"
B_SCRIPT="./b_root_setup.sh"
C_SCRIPT="./c_non_root_setup.sh"
#+++++++++++++++++++++++++++++++++++++++++++++++


# conf files++++++++++++++++++++++++++++++++++++ 
CONF_DIR="./base_conf_files"
LOCALE_GEN_FILE="$CONF_DIR/locale.gen"
LOCALE_FILE="$CONF_DIR/locale.conf"
HOSTNAME_FILE="$CONF_DIR/hostname"
HOSTS_FILE="$CONF_DIR/hosts"
GRUB_FILE="$CONF_DIR/grub"
MKINITCPIO_FILE="$CONF_DIR/mkinitcpio.conf"
#++++++++++++++++++++++++++++++++++++++++++++++++


# disks++++++++++++++++++++++++++++++++++++++++++
PARTITION_DISKS=true 
DISK_NAME=
SWAP_SIZE_MB='4096'


# if PARTITION_DISKS is true then 
# these values will be overwritten.
# otherwise, youll be prompted to fill out
# those that are empty when the script runs.
ROOT_PARTITION=
BOOT_PARTITION=

# additional parameters to be added to 
# GRUB_CMDLINE_LINUX after the required stuff
# to get the encrypted volume right. 
# e.g: root_trim=yes is a thing for ssd's 
ADDITIONAL_GRUB_PARAMS=
#++++++++++++++++++++++++++++++++++++++++++++++++


# user & host info++++++++++++++++++++++++++++++++++++++
USER_NAME='solid'
CHOSEN_HOSTNAME='malloc'

# "REGION/CITY" format
TIMEZONE=

# look in /etc/locales.gen to see what locales you'd like
# to have, and include them in this list
CHOSEN_LOCALES=( "en_US.UTF-8 UTF-8" "en_US ISO-8859-1" )
CHOSEN_LANG='en_US.UTF-8'
#++++++++++++++++++++++++++++++++++++++++++++++++

# pacman++++++++++++++++++++++++++++++++++++++++++
MIRROR_COUNT=50

# by default the following will be included up
# to the initial root section of the install:
#
# efibootmgr grub sudo vi vim zsh dosfstools
# p7zip git linux-headers
ADDITIONAL_SOFTWARE_INITIAL_ROOT=

# amd-ucode or intel-ucode
MICROCODE_PACKAGE=

# options here are
# xf86-video-fbdev
# xf86-video-vesa
# xf86-video-amdgpu
# xf86-video-ati
# xf86-video-intel
# xf86-video-nouveau
# nvidia
VIDEO_DRIVER=
#++++++++++++++++++++++++++++++++++++++++++++++++

# user desktop setup+++++++++++++++++++++++++++++
USER_SCRIPTS_DIR="/home/$USER_NAME/.local/bin"
BUILD_CLANG_FORMAT=true
BUILD_CLAG_OPTIONS="-j8"
#++++++++++++++++++++++++++++++++++++++++++++++++




#++++++++++++++++++++++++++++++++++++++++++++++++
# create gitignore 
GIT_IGNORE_FILE="./.gitignore"
