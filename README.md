# auto-arch-install
automatically (sort of) install arch linux the way i like it. before you try it, i encourage you to read the arch wiki's entry for your computer's model (if available) to check if there's anything you might need to add (you might be able to do it from one of the script's variables).

## who is this for?
mostly for me, but i would really appreciate it if someone else tried it on their own machine and/or gave me some feedback on it.

## what are the system requirements?
to have a system that supports GPT and an EFI partition scheme (`boot` and `root` partitions in this case). MBR support might be coming eventually.   


## instructions
the scripts need to be run from a live arch environment. to make full use of the scripts (such as the auto-partition functionality), this live environment must not rely on the disks on which you are planning to install your arch system. therefore, i'd recommend you use something like a live USB. for instance, copying this repo into the live arch USB installation medium (possibly by way of pluggin another USB with the repo into the system and copying) and running it from there might work, provided it contains the required packages by the time you are trying this guide out. 

failing that, you can create your own live USB arch environment. just make sure you install the following packages:
- `reflector`
- `arch-install-scripts`
- `dosfstools`
- `networkmanager` (only to make your life easier)

remember to check the [arch wiki's entry](https://wiki.archlinux.org/title/Install_Arch_Linux_on_a_removable_medium) on how to tweak a live USB environment. as of writing this, i suggest you:
- change the position of `block` and `keyboard` in your `mkinitcpio.conf` file to BEFORE `autodetect`
- install both `amd-ucode` and `intel-ucode` before you generate the `mkinitcpio`.
- run the `grub-install` command with `--recheck` and `--removable`

once you have fulfilled the above conditions, you can edit the contents of `config.sh` and run `a_pre_install.sh`.